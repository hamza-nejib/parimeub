create database if not exists parimeub;
create user IF NOT EXISTS 'java'@'%' identified by '58A}!^3K_P1t6i';
grant all privileges on parimeub.* to 'java'@'%';
flush privileges;
use parimeub;

/*==============================================================*/
/* Nom de SGBD :  MySQL 5.0                                     */
/* Date de création :  22-May-24 10:41:30 AM                    */
/*==============================================================*/

drop table if exists CATEGORIE;

drop table if exists CLIENT;

drop table if exists COMMANDE;

drop table if exists CONTIENT;

drop table if exists FACTURE;

drop table if exists MEUBLE;

/*==============================================================*/
/* Table : CATEGORIE                                            */
/*==============================================================*/
create table CATEGORIE
(
   codecategorie        int not null auto_increment,
   nomcategorie         varchar(20),
   primary key (codecategorie)
);

/*==============================================================*/
/* Table : CLIENT                                               */
/*==============================================================*/
create table CLIENT
(
   codeclient           int not null auto_increment,
   nom                  varchar(40),
   prenom               varchar(40),
   telephone            varchar(20),
   email                varchar(40),
   adresse              varchar(150),
   password             VARCHAR(255),
   primary key (codeclient)
);

/*==============================================================*/
/* Table : COMMANDE                                             */
/*==============================================================*/
create table COMMANDE
(
   codecommande         int not null auto_increment,
   codeclient           int not null,
   datecommande         date,
   primary key (codecommande)
);

/*==============================================================*/
/* Table : CONTIENT                                             */
/*==============================================================*/
create table CONTIENT
(
   codemeuble           int not null,
   codecommande         int not null,
   quantitemeuble       int,
   primary key (codemeuble, codecommande)
);

/*==============================================================*/
/* Table : FACTURE                                              */
/*==============================================================*/
create table FACTURE
(
   codefacture          int not null auto_increment,
   codecommande         int not null,
   datefacture          date,
   primary key (codefacture)
);

/*==============================================================*/
/* Table : MEUBLE                                               */
/*==============================================================*/
create table MEUBLE
(
   codemeuble           int not null auto_increment,
   codecategorie        int not null,
   libelle              varchar(40),
   dimension            varchar(30),
   prix                 float(10,0),
   etat                 varchar(20),
   image                longblob,
   description          text,
   primary key (codemeuble)
);
SET FOREIGN_KEY_CHECKS=0; /* pour resoudre le probleme de clé étrangère*/
alter table COMMANDE add constraint FK_PASSE foreign key (codeclient)
      references CLIENT (codeclient) on delete restrict on update restrict;

alter table CONTIENT add constraint FK_CONTIENT foreign key (codecommande)
      references COMMANDE (codecommande) on delete restrict on update restrict;

alter table CONTIENT add constraint FK_CONTIENT2 foreign key (codemeuble)
      references MEUBLE (codemeuble) on delete restrict on update restrict;

alter table FACTURE add constraint FK_CORRESPOND foreign key (codecommande)
      references COMMANDE (codecommande) on delete restrict on update restrict;

alter table MEUBLE add constraint FK_APPARTIENT foreign key (codecategorie)
      references CATEGORIE (codecategorie) on delete restrict on update restrict;
