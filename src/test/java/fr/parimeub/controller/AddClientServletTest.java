package fr.parimeub.controller;
/*
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import fr.parimeub.entity.Client;
import fr.parimeub.persistence.ClientDao;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
*/
public class AddClientServletTest {
	/*
	@Mock
    private HttpServletRequest request;

    @Mock
    private HttpServletResponse response;

    @Mock
    private HttpSession session;

    @Mock
    private ClientDao clientDao;

    @InjectMocks
    private AddClientServlet servlet;

    @Before
    public void setUp() {
    	MockitoAnnotations.openMocks(this);
    }

    @Test
    public void testDoPostSuccess() throws Exception {
        // Mocking request parameters
        when(request.getParameter("nom")).thenReturn("John");
        when(request.getParameter("prenom")).thenReturn("Doe");
        when(request.getParameter("telephone")).thenReturn("1234567890");
        when(request.getParameter("email")).thenReturn("john.doe@example.com");
        when(request.getParameter("adresse")).thenReturn("123 Street");
        when(request.getParameter("password")).thenReturn("password123");

        // Mocking session
        when(request.getSession()).thenReturn(session);

        // Mocking ClientDao behavior
        when(clientDao.addClient(any(Client.class))).thenReturn(true);

        // Call the doPost method
        servlet.doPost(request, response);

        // Verify the interactions and assert the expected behavior
        verify(session).setAttribute("success", "Votre compte a été créé avec succès !!");
        verify(response).sendRedirect("connecter.jsp");
    }*/

    /*@Test
    public void testDoPostFailure() throws Exception {
        // Mocking request parameters
        when(request.getParameter("nom")).thenReturn("John");
        when(request.getParameter("prenom")).thenReturn("Doe");
        when(request.getParameter("telephone")).thenReturn("1234567890");
        when(request.getParameter("email")).thenReturn("john.doe@example.com");
        when(request.getParameter("adresse")).thenReturn("123 Street");
        when(request.getParameter("password")).thenReturn("password123");

        // Mocking session
        when(request.getSession()).thenReturn(session);

        // Mocking ClientDao behavior
        when(clientDao.addClient(any(Client.class))).thenReturn(false);

        // Call the doPost method
        servlet.doPost(request, response);

        // Verify the interactions and assert the expected behavior
        verify(session).setAttribute("error", "Échec lors du processus d'inscription. Veuillez réessayer  !!");
        verify(response).sendRedirect("creer.jsp");
    }*/
}
