package fr.parimeub.entity;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class ClientTest {

    @Test
    public void testClientConstructorAndGetters() {
        Client client = new Client("John", "Doe", "0606060606", "john.doe@example.com", "rue de paris", "password123");

        assertEquals("John", client.getNom());
        assertEquals("Doe", client.getPrenom());
        assertEquals("0606060606", client.getTelephone());
        assertEquals("john.doe@example.com", client.getEmail());
        assertEquals("rue de paris", client.getAdresse());
        assertEquals("password123", client.getPassword());
    }

    @Test
    public void testSetters() {
        Client client = new Client("John", "Doe", "0606060606", "john.doe@example.com", "rue de paris", "password123");

        client.setNom("Jane");
        client.setPrenom("Smith");
        client.setTelephone("0707070707");
        client.setEmail("jane.smith@example.com");
        client.setAdresse("avenue de paris");
        client.setPassword("newpassword123");

        assertEquals("Jane", client.getNom());
        assertEquals("Smith", client.getPrenom());
        assertEquals("0707070707", client.getTelephone());
        assertEquals("jane.smith@example.com", client.getEmail());
        assertEquals("avenue de paris", client.getAdresse());
        assertEquals("newpassword123", client.getPassword());
    }
}
