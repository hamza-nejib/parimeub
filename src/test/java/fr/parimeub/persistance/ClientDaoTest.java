package fr.parimeub.persistance;

import static org.junit.Assert.assertFalse;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.when;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import fr.parimeub.entity.Client;
import fr.parimeub.persistence.ClientDao;

public class ClientDaoTest {
	
	@Mock
    private Connection mockConnection;

    @Mock
    private PreparedStatement mockPreparedStatement;

    @InjectMocks
    private ClientDao clientDao;

    @Before
    public void setUp() throws Exception {
    	MockitoAnnotations.openMocks(this);
    }

    /*@Test
    public void testAddClientSuccess() throws Exception {
        // Prepare the mock behavior
        when(mockConnection.prepareStatement(anyString())).thenReturn(mockPreparedStatement);

        // Mock the Client object
        Client client = new Client("John", "Doe", "1234567890", "john.doe@example.com", "123 Street", "password123");

        // Call the method under test
        boolean result = clientDao.addClient(client);

        // Verify the interactions and assert the expected behavior
        verify(mockPreparedStatement).setString(1, client.getNom());
        verify(mockPreparedStatement).setString(2, client.getPrenom());
        verify(mockPreparedStatement).setString(3, client.getTelephone());
        verify(mockPreparedStatement).setString(4, client.getEmail());
        verify(mockPreparedStatement).setString(5, client.getAdresse());
        verify(mockPreparedStatement).setString(6, PasswordUtil.hashPassword(client.getPassword()));
        verify(mockPreparedStatement).executeUpdate();

        assertTrue(result);
    }*/

    @Test
    public void testAddClientFailure() throws Exception {
        // Prepare the mock behavior to throw SQLException
        when(mockConnection.prepareStatement(anyString())).thenReturn(mockPreparedStatement);
        doThrow(new SQLException()).when(mockPreparedStatement).executeUpdate();

        // Mock the Client object
        Client client = new Client("John", "Doe", "1234567890", "john.doe@example.com", "123 Street", "password123");

        // Call the method under test
        boolean result = clientDao.addClient(client);

        // Verify the interactions and assert the expected behavior
        assertFalse(result);
    }
}