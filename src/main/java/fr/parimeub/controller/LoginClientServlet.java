package fr.parimeub.controller;

import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;

import java.io.IOException;

import fr.parimeub.db.PasswordUtil;
import fr.parimeub.entity.Client;
import fr.parimeub.persistence.ClientDao;

@WebServlet("/LoginClient")
public class LoginClientServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
	    String email = request.getParameter("email");
	    String password = request.getParameter("password");
	    
	    ClientDao clientDao = new ClientDao();
	    Client user = clientDao.checkLogin(email);
	    if (user != null && PasswordUtil.checkPassword(password, user.getPassword())) {
	        HttpSession session = request.getSession();
	        session.setAttribute("oklog", "Bonjour "+user.getNom());
	        response.sendRedirect("index.jsp");
	    }else {
	    	HttpSession session = request.getSession();
	    	session.setAttribute("kolog", "E-mail ou mot de passe incorrect. Veuillez réessayer !!");
	    	response.sendRedirect("connecter.jsp");
	    }
	}
}
