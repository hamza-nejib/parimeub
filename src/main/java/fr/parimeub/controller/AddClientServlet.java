package fr.parimeub.controller;

import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.io.IOException;
import fr.parimeub.entity.Client;
import fr.parimeub.persistence.ClientDao;

@WebServlet("/AddClient")
public class AddClientServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
   
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	
		String nom = request.getParameter("nom");
	    String prenom = request.getParameter("prenom");
	    String telephone = request.getParameter("telephone");
	    String email = request.getParameter("email");
	    String adresse = request.getParameter("adresse");
	    String password = request.getParameter("password");
	    
	    Client newClient = new Client(nom, prenom, telephone, email, adresse, password);
	    ClientDao clientDao = new ClientDao();
	    if(clientDao.addClient(newClient)) {
	    	HttpSession session = request.getSession();
		    session.setAttribute("success", "Votre compte a été créé avec succès !!");
		    response.sendRedirect("connecter.jsp");
	    } else {
	    	HttpSession session = request.getSession();
		    session.setAttribute("error", "Échec lors du processus d'inscription. Veuillez réessayer  !!");
		    response.sendRedirect("creer.jsp");
	    }
	}
}
