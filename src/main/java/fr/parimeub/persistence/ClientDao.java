package fr.parimeub.persistence;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import fr.parimeub.db.ConnectDb;
import fr.parimeub.db.PasswordUtil;
import fr.parimeub.entity.Client;

public class ClientDao {
	Connection connection;
	public ClientDao() {
		try {
			connection = ConnectDb.getConnection();
		} catch (ClassNotFoundException|SQLException e) {
			e.printStackTrace();
		} 
	}
	public boolean addClient(Client client) {
		String hashedPassword = PasswordUtil.hashPassword(client.getPassword());
		String sql = "INSERT INTO CLIENT (nom, prenom, telephone, email, adresse, password) VALUES (?, ?, ?, ?, ?, ?)";
		try {
			
			PreparedStatement prst = connection.prepareStatement(sql);
			prst.setString(1, client.getNom());
	        prst.setString(2, client.getPrenom());
	        prst.setString(3, client.getTelephone());
	        prst.setString(4, client.getEmail());
	        prst.setString(5, client.getAdresse());
	        prst.setString(6, hashedPassword);
	        prst.executeUpdate();
	        return true;
		}catch(SQLException e) {
	  	    e.printStackTrace();
	  	    return false;
		}
	}
	
	public Client checkLogin(String email) {
		Client user = null;
		String sql = "SELECT nom, email, password FROM CLIENT WHERE email = ?";
		try {
			PreparedStatement prst = connection.prepareStatement(sql);
			prst.setString(1, email);
			ResultSet result = prst.executeQuery();
			if (result.next()) {
				user = new Client();
				user.setNom(result.getString("nom"));
				user.setEmail(result.getString("email"));
				user.setPassword(result.getString("password"));
			}
		}catch(SQLException e) {
			e.printStackTrace();
		}
		return user;
	}
}
