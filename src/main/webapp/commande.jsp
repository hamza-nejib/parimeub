<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>   
 
<!DOCTYPE html>
<html lang="fr">

<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title>PariMeub</title>
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.11.2/css/all.css">
  <link href="css/bootstrap.min.css" rel="stylesheet">
  <link href="css/mdb.min.css" rel="stylesheet">
  <link href="css/style.min.css" rel="stylesheet">
  <link rel="shortcut icon" type="image/x-icon" href="./img/logo.png" />
  <style type="text/css">
    html, body, header, .carousel {height: 60vh;}
	@media (max-width: 740px) {html, body, header, .carousel {height: 100vh;}}
    @media (min-width: 800px) and (max-width: 850px) {html, body, header, .carousel {height: 100vh;}}
  </style>
</head>

<body>
  <!-- Navbar -->
  <nav class="navbar fixed-top navbar-expand-lg navbar-light white scrolling-navbar">
    <div class="container">
      <!-- Brand -->
      <a href="./index.jsp"><img id="logo" src="./img/logo.png"></a>
      <a class="navbar-brand waves-effect" href="./index.jsp"><strong style="color:#593718; font-weight: bold;" >PariMeub</strong></a>
      <!-- Collapse -->
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
        aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <!-- Links -->
      <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <!-- Left -->
        <ul class="navbar-nav mr-auto">
          <li class="nav-item"><a class="nav-link waves-effect" href="./index.jsp">Accueil</a></li>
          <li class="nav-item"><a class="nav-link waves-effect" href="./meubles.jsp">Meubles</a></li>
          <li class="nav-item"><a class="nav-link waves-effect" href="./profil.jsp">Mon Profil</a></li>
          <li class="nav-item"><a class="nav-link waves-effect" href="./commande.jsp">Contact</a></li>
          <li class="nav-item"><a class="nav-link waves-effect" href="./connecter.jsp">Se Connecter</a></li>
        </ul>
        <!-- Right -->
        <ul class="navbar-nav nav-flex-icons">
          <li class="nav-item"><a class="nav-link waves-effect" href="./commande.jsp"><span class="badge brown z-depth-1 mr-1"> 1 </span>
              <i class="fas fa-shopping-cart"></i><span class="clearfix d-none d-sm-inline-block"> Panier </span></a>
          </li>
          <li class="nav-item">
            <a href="https://www.facebook.com/" class="nav-link waves-effect" target="_blank"><i class="fab fa-facebook-f"></i></a>
          </li>
          <li class="nav-item">
            <a href="https://twitter.com" class="nav-link waves-effect" target="_blank"><i class="fab fa-twitter"></i></a>
          </li>
          <li class="nav-item"><a href="https://gitlab.com/hamza-nejib/parimeub" class="nav-link border border-light rounded waves-effect"
              target="_blank">Code<i class="fab fa-gitlab"></i></a>
          </li>
        </ul>
        <% String oklog = (String)session.getAttribute("oklog");
			if(oklog != null){ %>
		<ul class="navbar-nav mr-auto">
		  <li class="nav-item">
		  	<strong class="nav-link waves-effect" style="color:#593718; font-weight: bold;" ><%=oklog%></strong>
		  </li>
		  <li class="nav-item"><a class="nav-link waves-effect" href="./LogoutClient">Se Déconnecter</a></li>
		</ul>
		  <%}%>
      </div>
    </div>
  </nav>
  <!-- Navbar -->

  <!--Main layout-->
  <main class="mt-5 pt-4">
    <div class="container wow fadeIn">
      <!-- Heading -->
      <h2 style="margin: 1.5rem" class=" h2 text-center">Payer mes achats</h2>
      <!--Grid row-->
      <div class="row">
        <!--Grid column-->
        <div class="col-md-8 mb-4">
          <!--Card-->
          <div class="card">
            <!--Card content-->
            <form class="card-body">
              <!--Grid row-->
              <div class="row">
                <!--Grid column-->
                <div class="col-md-6 mb-2">
                  <!--firstName-->
                  <div class="md-form" style="margin:5px;">
                    <input type="text" id="firstName" class="form-control" required>
                    <label for="firstName" class="">Nom</label>
                  </div>
                </div>
                <!--Grid column-->
                <!--Grid column-->
                <div class="col-md-6 mb-2">
                  <!--lastName-->
                  <div class="md-form" style="margin:5px;">
                    <input type="text" id="lastName" class="form-control" required>
                    <label for="lastName" class="">Prénom</label>
                  </div>
                </div>
                <!--Grid column-->
              </div>
              <!--Grid row-->
              <!--email-->
              <div class="md-form mb-5" style="margin:5px;">
                <input type="text" id="email" class="form-control">
                <label for="email" class="">Email</label>
              </div>

              <!--address-->
              <div class="md-form mb-5" style="margin:5px;">
                <input type="text" id="address" class="form-control">
                <label for="address" class="">Adresse</label>
              </div>
              <!--Grid row-->
              <div class="row" style="margin-top:10px;">
                <!--Grid column-->
                <div class="col-lg-4 col-md-12 mb-4">
                  <label for="country">Pays</label>
                  <select class="custom-select d-block w-100" id="country" required>
                    <option value="">Choisir...</option>
                    <option>France</option>
                  </select>
                  <div class="invalid-feedback">
                    Veuillez sélectionner votre pays.
                  </div>
                </div>
                <!--Grid column-->
                <!--Grid column-->
                <div class="col-lg-4 col-md-6 mb-4">
                  <label for="state">Ville</label>
                  <select class="custom-select d-block w-100" id="state" required>
                    <option value="">Choisir...</option>
                    <option>Paris</option>
                    <option>Marseille</option>
                    <option>Lille</option>
                  </select>
                  <div class="invalid-feedback">
                    Veuillez sélectionner votre ville.
                  </div>
                </div>
                <!--Grid column-->
                <!--Grid column-->
                <div class="col-lg-4 col-md-6 mb-4">

                  <label for="zip">Code Postal</label>
                  <input type="text" class="form-control" id="zip" placeholder="" required>
                  <div class="invalid-feedback">
                    Code postal obligatoire.
                  </div>
                </div>
                <!--Grid column-->
              </div>
              <!--Grid row-->           
              <div class="d-block my-3" style="margin:5px;">
                <div class="custom-control custom-radio">
                  <input id="credit" name="paymentMethod" type="radio" class="custom-control-input" checked required>
                  <label class="custom-control-label" for="credit">Carte Bancaire</label>
                </div>              
                <div class="custom-control custom-radio">
                  <input id="paypal" name="paymentMethod" type="radio" class="custom-control-input" required>
                  <label class="custom-control-label" for="paypal">Paypal</label>
                </div>
              </div>
              <div class="row">
                <div class="col-md-6 mb-3">
                  <label for="cc-name">Titulaire de la carte</label>
                  <input type="text" class="form-control" id="cc-name" placeholder="" required>
                  <div class="invalid-feedback">
                    Le nom du titulaire est obligatoire
                  </div>
                </div>
              </div>
               <!--Grid row-->
              <div class="row">
                <!--Grid column-->
                <div class="col-md-6 mb-3">
                  <label for="cc-number">Numéro de la carte</label>
                  <input type="text" class="form-control" id="cc-number" placeholder="" required>
                  <div class="invalid-feedback">
                    Le numéro de la carte est obligatoire
                  </div>
                </div>
                <div class="col-md-3 mb-3">
                  <label for="cc-expiration">Expiration</label>
                  <input type="text" class="form-control" id="cc-expiration" placeholder="" required>
                  <div class="invalid-feedback">
                    La date d'expiration est obligatoire
                  </div>
                </div>
                <div class="col-md-3 mb-3">
                  <label for="cc-expiration">CVV</label>
                  <input type="text" class="form-control" id="cc-cvv" placeholder="" required>
                  <div class="invalid-feedback">
                    Le code de sécurité est obligatoire
                  </div>
                </div>
                </div>
                <!--Grid column-->
              <!--Grid row-->
              <button style="display:flex; border: 0;padding: 15px;width: 60%; color: #fff; background :#c3986b;font-size: 18px;
              border-radius: 30px; outline: none; cursor: pointer; margin: 20px auto 0; justify-content: center; 
              align-items: center;" type="submit">Valider le paiement</button>
            </form>
          </div>
          <!--/.Card-->
        </div>
        <!--Grid column-->
        <!--Grid column-->
        <div class="col-md-4 mb-4">
          <!-- Heading -->
          <h4 class="d-flex justify-content-between align-items-center mb-3">
            <span class="text-muted">Mon Panier</span>
            <span class="badge brown badge-pill">2</span>
          </h4>
          <!-- Cart -->
          <ul class="list-group mb-3 z-depth-1">
            <li class="list-group-item d-flex justify-content-between lh-condensed">
              <div>
                <h6 class="my-0">Table de nuit</h6>
              </div>
              <span class="text-muted">40€</span>
            </li>
            <li class="list-group-item d-flex justify-content-between lh-condensed">
              <div>
                <h6 class="my-0">Chaise</h6>
              </div>
              <span class="text-muted">20€</span>
            </li>
            <li class="list-group-item d-flex justify-content-between bg-light">
              <div style="color:#c3986b;">
                <h6 class="my-0">Code Promo</h6>
                <small>parimeub2024</small>
              </div>
              <span style="color:#c3986b;">-10€</span>
            </li>
            <li class="list-group-item d-flex justify-content-between">
              <span>Total (EUR)</span>
              <strong>50€</strong>
            </li>
          </ul>
          <!-- Cart -->

          <!-- Promo code -->
          <form class="card p-2">
            <div class="input-group">
              <input type="text" class="form-control" placeholder="Code Promo" aria-label="Recipient's username" aria-describedby="basic-addon2">
              <div class="input-group-append">
                <button class="btn btn-md waves-effect m-0" type="button">Utiliser</button>
              </div>
            </div>
          </form>
          <!-- Promo code -->

        </div>
        <!--Grid column-->

      </div>
      <!--Grid row-->

    </div>
  </main>
  <!--Main layout-->

  <!--Footer-->
  <footer class="page-footer text-center font-small mt-4 wow fadeIn" style="background-color: #c3986b;">
    <!--Call to action-->
    <div class="pt-4">
      <ul class="navbar-nav mr-auto">
          <li class="nav-item"><a class="nav-link waves-effect" href="#">À propos</a></li>
          <li class="nav-item"><a class="nav-link waves-effect" href="#">Moyens de Paiement</a></li>
          <li class="nav-item"><a class="nav-link waves-effect" href="#">Nous Contacter</a></li>
          <li class="nav-item"><a class="nav-link waves-effect" href="#">Mentions Légales</a></li>
      </ul>
    </div>
    <!--/.Call to action-->
    <hr class="my-4">
    <!-- Social icons -->
    <div class="pb-4">
      <a href="https://www.facebook.com/" target="_blank"><i class="fab fa-facebook-f mr-3"></i></a>
	  <a href="https://twitter.com/" target="_blank"><i class="fab fa-twitter mr-3"></i></a>
      <a href="https://www.youtube.com/" target="_blank"><i class="fab fa-youtube mr-3"></i></a>
      <a href="https://plus.google.com/" target="_blank"><i class="fab fa-google-plus-g mr-3"></i></a>
      <a href="https://dribbble.com/" target="_blank"><i class="fab fa-dribbble mr-3"></i></a>
      <a href="https://pinterest.com/" target="_blank"><i class="fab fa-pinterest mr-3"></i></a>
      <a href="https://gitlab.com/hamza-nejib/parimeub" target="_blank"><i class="fab fa-gitlab mr-3"></i></a>
    </div>
    <!-- Social icons -->
    <!--Copyright-->
    <div class="footer-copyright py-3">&copy; 2024 Copyright:<a href="https://hamza-nejib.github.io/hn/" target="_blank"> Hamza NEJIB </a></div>
    <!--/.Copyright-->
  </footer>
  <!--/.Footer-->

  <!-- SCRIPTS -->
  <script type="text/javascript" src="js/jquery-3.4.1.min.js"></script>
  <script type="text/javascript" src="js/popper.min.js"></script>
  <script type="text/javascript" src="js/bootstrap.min.js"></script>
  <script type="text/javascript" src="js/mdb.min.js"></script>
  <script type="text/javascript">new WOW().init();</script>
</body>
</html>