<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>    
<!DOCTYPE html>
<html lang="fr">

<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title>PariMeub</title>
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.11.2/css/all.css">
  <link href="css/bootstrap.min.css" rel="stylesheet">
  <link href="css/mdb.min.css" rel="stylesheet">
  <link href="css/style.min.css" rel="stylesheet">
  <link rel="shortcut icon" type="image/x-icon" href="./img/logo.png" />
  <style type="text/css">
    html, body, header, .carousel {height: 60vh;}
	@media (max-width: 740px) {html, body, header, .carousel {height: 100vh;}}
    @media (min-width: 800px) and (max-width: 850px) {html, body, header, .carousel {height: 100vh;}}
  </style>
</head>

<body>
  <!-- Navbar -->
  <nav class="navbar fixed-top navbar-expand-lg navbar-light white scrolling-navbar">
    <div class="container">
      <!-- Brand -->
      <a href="./index.jsp"><img id="logo" src="./img/logo.png"></a>
      <a class="navbar-brand waves-effect" href="./index.jsp"><strong style="color:#593718; font-weight: bold;" >PariMeub</strong></a>
      <!-- Collapse -->
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
        aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <!-- Links -->
      <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <!-- Left -->
        <ul class="navbar-nav mr-auto">
          <li class="nav-item"><a class="nav-link waves-effect" href="./index.jsp">Accueil</a></li>
          <li class="nav-item active"><a class="nav-link waves-effect" href="./meubles.jsp">Meubles<span class="sr-only">(current)</span></a></li>
          <li class="nav-item"><a class="nav-link waves-effect" href="./profil.jsp">Mon Profil</a></li>
          <li class="nav-item"><a class="nav-link waves-effect" href="./contact.jsp">Contact</a></li>
          <li class="nav-item"><a class="nav-link waves-effect" href="./connecter.jsp">Se Connecter</a></li>
        </ul>
        <!-- Right -->
        <ul class="navbar-nav nav-flex-icons">
          <li class="nav-item"><a class="nav-link waves-effect" href="./commande.jsp"><span class="badge brown z-depth-1 mr-1"> 1 </span>
              <i class="fas fa-shopping-cart"></i><span class="clearfix d-none d-sm-inline-block"> Panier </span></a>
          </li>
          <li class="nav-item">
            <a href="https://www.facebook.com/" class="nav-link waves-effect" target="_blank"><i class="fab fa-facebook-f"></i></a>
          </li>
          <li class="nav-item">
            <a href="https://twitter.com" class="nav-link waves-effect" target="_blank"><i class="fab fa-twitter"></i></a>
          </li>
          <li class="nav-item"><a href="https://gitlab.com/hamza-nejib/parimeub" class="nav-link border border-light rounded waves-effect"
              target="_blank">Code<i class="fab fa-gitlab"></i></a>
          </li>
        </ul>
        <% String oklog = (String)session.getAttribute("oklog");
			if(oklog != null){ %>
		<ul class="navbar-nav mr-auto">
		  <li class="nav-item">
		  	<strong class="nav-link waves-effect" style="color:#593718; font-weight: bold;" ><%=oklog%></strong>
		  </li>
		  <li class="nav-item"><a class="nav-link waves-effect" href="./LogoutClient">Se Déconnecter</a></li>
		</ul>
		  <%}%>
      </div>
    </div>
  </nav>
  <!-- Navbar -->
  
  <!--Main layout-->
  <main>
    <div class="container" style="margin-top:9vh;">
      <!--Section: Products v.3-->
      <section class="text-center mb-4">

        <!--Grid row-->
        <div class="row wow fadeIn">

          <!--Grid column-->
          <div class="col-lg-3 col-md-6 mb-4">

            <!--Card-->
            <div class="card">
              <!--Card image-->
              <div class="view overlay">
                <img src="./img/chaise.png" class="card-img-top" alt="">
                <a> <span class="mask rgba-white-slight"> </span> </a>
              </div>
              <!--Card image-->
              <!--Card content-->
              <div class="card-body text-center">
                <!--Catégory & Titre-->
                <h5><a href="" style="color:#593718;">Chaises</a></h5>              
                <h5><strong><a href="" style="color:#c3986b;">Chaise de bureau</a></strong></h5>
                <h4 style="color:#c3986b;"><strong>50€</strong></h4>
              </div>
              <!--Card content-->
            </div>
            <!--Card-->

          </div>
          <!--Grid column-->

          <!--Grid column-->
          <div class="col-lg-3 col-md-6 mb-4">

            <!--Card-->
            <div class="card">
              <!--Card image-->
              <div class="view overlay">
                <img src="./img/lit.png" class="card-img-top" alt="">
                <a><span class="mask rgba-white-slight"></span></a>
              </div>
              <!--Card image-->
              <!--Card content-->
              <div class="card-body text-center">
                <!--Catégorie & Titre-->                
                <h5><a href="" style="color:#593718;">Literie</a></h5>
                <h5><strong><a href="" style="color:#c3986b;">Lit 2 places</a></strong></h5>
                <h4 style="color:#c3986b;"><strong>250€</strong></h4>
              </div>
              <!--Card content-->
            </div>
            <!--Card-->

          </div>
          <!--Grid column-->

          <!--Grid column-->
          <div class="col-lg-3 col-md-6 mb-4">

            <!--Card-->
            <div class="card">
              <!--Card image-->
              <div class="view overlay">
                <img src="./img/table.png" class="card-img-top" alt="">
                <a><span class="mask rgba-white-slight"></span></a>
              </div>
              <!--Card image-->
              <!--Card content-->
              <div class="card-body text-center">
                <!--Catégorie & Titre-->
                <h5><a href="" style="color:#593718;">Tables</a></h5>
                <h5><strong><a href="" style="color:#c3986b;">Table recangule</a></strong></h5>
                <h4 style="color:#c3986b;"><strong>60€</strong></h4>
              </div>
              <!--Card content-->
            </div>
            <!--Card-->

          </div>
          <!--Grid column-->

          <!--Fourth column-->
          <div class="col-lg-3 col-md-6 mb-4">

            <!--Card-->
            <div class="card">
              <!--Card image-->
              <div class="view overlay">
                <img src="./img/armoir.png" class="card-img-top" alt="">
                <a><span class="mask rgba-white-slight"></span></a>
              </div>
              <!--Card image-->
              <!--Card content-->
              <div class="card-body text-center">
                <!--Catégorie & Titre-->
                <h5><a href="" style="color:#593718;">Rangement</a></h5>           
                <h5><strong><a href="" style="color:#c3986b;">Armoire</a></strong></h5>             
                <h4 style="color:#c3986b;"><strong>200€</strong></h4>
              </div>
              <!--Card content-->
            </div>
            <!--Card-->
          </div>
          <!--Fourth column-->

        </div>
        <!--Grid row-->

        <!--Grid row-->
        <div class="row wow fadeIn">

          <!--Grid column-->
          <div class="col-lg-3 col-md-6 mb-4">

            <!--Card-->
            <div class="card">
              <!--Card image-->
              <div class="view overlay">
                <img src="./img/lit.png" class="card-img-top" alt="">
                <a><span class="mask rgba-white-slight"></span></a>
              </div>
              <!--Card image-->
              <!--Card content-->
              <div class="card-body text-center">
                <!--Catégorie & Titre-->                
                <h5><a href="" style="color:#593718;">Literie</a></h5>
                <h5><strong><a href="" style="color:#c3986b;">Lit 2 places</a></strong></h5>
                <h4 style="color:#c3986b;"><strong>250€</strong></h4>
              </div>
              <!--Card content-->
            </div>
            <!--Card-->

          </div>
          <!--Grid column-->

          <!--Grid column-->
          <div class="col-lg-3 col-md-6 mb-4">

            <!--Card-->
            <div class="card">
              <!--Card image-->
              <div class="view overlay">
                <img src="./img/table.png" class="card-img-top" alt="">
                <a><span class="mask rgba-white-slight"></span></a>
              </div>
              <!--Card image-->
              <!--Card content-->
              <div class="card-body text-center">
                <!--Catégorie & Titre-->
                <h5><a href="" style="color:#593718;">Tables</a></h5>
                <h5><strong><a href="" style="color:#c3986b;">Table recangule</a></strong></h5>
                <h4 style="color:#c3986b;"><strong>60€</strong></h4>
              </div>
              <!--Card content-->
            </div>
            <!--Card-->

          </div>
          <!--Grid column-->

          <!--Grid column-->
          <div class="col-lg-3 col-md-6 mb-4">

            <!--Card-->
            <div class="card">
              <!--Card image-->
              <div class="view overlay">
                <img src="./img/armoir.png" class="card-img-top" alt="">
                <a><span class="mask rgba-white-slight"></span></a>
              </div>
              <!--Card image-->
              <!--Card content-->
              <div class="card-body text-center">
                <!--Catégorie & Titre-->
                <h5><a href="" style="color:#593718;">Rangement</a></h5>           
                <h5><strong><a href="" style="color:#c3986b;">Armoire</a></strong></h5>             
                <h4 style="color:#c3986b;"><strong>200€</strong></h4>
              </div>
              <!--Card content-->
            </div>
            <!--Card-->

          </div>
          <!--Grid column-->

          <!--Fourth column-->
          <div class="col-lg-3 col-md-6 mb-4">

            <!--Card-->
            <div class="card">
              <!--Card image-->
              <div class="view overlay">
                <img src="./img/chaise.png" class="card-img-top" alt="">
                <a> <span class="mask rgba-white-slight"> </span> </a>
              </div>
              <!--Card image-->
              <!--Card content-->
              <div class="card-body text-center">
                <!--Catégory & Titre-->
                <h5><a href="" style="color:#593718;">Chaises</a></h5>              
                <h5><strong><a href="" style="color:#c3986b;">Chaise de bureau</a></strong></h5>
                <h4 style="color:#c3986b;"><strong>50€</strong></h4>
              </div>
              <!--Card content-->
            </div>
            <!--Card-->
            

          </div>
          <!--Fourth column-->

        </div>
        <!--Grid row-->
        <!--Grid row-->
        <div class="row wow fadeIn">

          <!--Grid column-->
          <div class="col-lg-3 col-md-6 mb-4">

            <!--Card-->
            <div class="card">
              <!--Card image-->
              <div class="view overlay">
                <img src="./img/chaise.png" class="card-img-top" alt="">
                <a> <span class="mask rgba-white-slight"> </span> </a>
              </div>
              <!--Card image-->
              <!--Card content-->
              <div class="card-body text-center">
                <!--Catégory & Titre-->
                <h5><a href="" style="color:#593718;">Chaises</a></h5>              
                <h5><strong><a href="" style="color:#c3986b;">Chaise de bureau</a></strong></h5>
                <h4 style="color:#c3986b;"><strong>50€</strong></h4>
              </div>
              <!--Card content-->
            </div>
            <!--Card-->

          </div>
          <!--Grid column-->

          <!--Grid column-->
          <div class="col-lg-3 col-md-6 mb-4">

            <!--Card-->
            <div class="card">
              <!--Card image-->
              <div class="view overlay">
                <img src="./img/lit.png" class="card-img-top" alt="">
                <a><span class="mask rgba-white-slight"></span></a>
              </div>
              <!--Card image-->
              <!--Card content-->
              <div class="card-body text-center">
                <!--Catégorie & Titre-->                
                <h5><a href="" style="color:#593718;">Literie</a></h5>
                <h5><strong><a href="" style="color:#c3986b;">Lit 2 places</a></strong></h5>
                <h4 style="color:#c3986b;"><strong>250€</strong></h4>
              </div>
              <!--Card content-->
            </div>
            <!--Card-->

          </div>
          <!--Grid column-->

          <!--Grid column-->
          <div class="col-lg-3 col-md-6 mb-4">

            <!--Card-->
            <div class="card">
              <!--Card image-->
              <div class="view overlay">
                <img src="./img/table.png" class="card-img-top" alt="">
                <a><span class="mask rgba-white-slight"></span></a>
              </div>
              <!--Card image-->
              <!--Card content-->
              <div class="card-body text-center">
                <!--Catégorie & Titre-->
                <h5><a href="" style="color:#593718;">Tables</a></h5>
                <h5><strong><a href="" style="color:#c3986b;">Table recangule</a></strong></h5>
                <h4 style="color:#c3986b;"><strong>60€</strong></h4>
              </div>
              <!--Card content-->
            </div>
            <!--Card-->

          </div>
          <!--Grid column-->

          <!--Fourth column-->
          <div class="col-lg-3 col-md-6 mb-4">

            <!--Card-->
            <div class="card">
              <!--Card image-->
              <div class="view overlay">
                <img src="./img/armoir.png" class="card-img-top" alt="">
                <a><span class="mask rgba-white-slight"></span></a>
              </div>
              <!--Card image-->
              <!--Card content-->
              <div class="card-body text-center">
                <!--Catégorie & Titre-->
                <h5><a href="" style="color:#593718;">Rangement</a></h5>           
                <h5><strong><a href="" style="color:#c3986b;">Armoire</a></strong></h5>             
                <h4 style="color:#c3986b;"><strong>200€</strong></h4>
              </div>
              <!--Card content-->
            </div>
            <!--Card-->
          </div>
          <!--Fourth column-->

        </div>
        <!--Grid row-->

        <!--Grid row-->
        <div class="row wow fadeIn">

          <!--Grid column-->
          <div class="col-lg-3 col-md-6 mb-4">

            <!--Card-->
            <div class="card">
              <!--Card image-->
              <div class="view overlay">
                <img src="./img/lit.png" class="card-img-top" alt="">
                <a><span class="mask rgba-white-slight"></span></a>
              </div>
              <!--Card image-->
              <!--Card content-->
              <div class="card-body text-center">
                <!--Catégorie & Titre-->                
                <h5><a href="" style="color:#593718;">Literie</a></h5>
                <h5><strong><a href="" style="color:#c3986b;">Lit 2 places</a></strong></h5>
                <h4 style="color:#c3986b;"><strong>250€</strong></h4>
              </div>
              <!--Card content-->
            </div>
            <!--Card-->

          </div>
          <!--Grid column-->

          <!--Grid column-->
          <div class="col-lg-3 col-md-6 mb-4">

            <!--Card-->
            <div class="card">
              <!--Card image-->
              <div class="view overlay">
                <img src="./img/table.png" class="card-img-top" alt="">
                <a><span class="mask rgba-white-slight"></span></a>
              </div>
              <!--Card image-->
              <!--Card content-->
              <div class="card-body text-center">
                <!--Catégorie & Titre-->
                <h5><a href="" style="color:#593718;">Tables</a></h5>
                <h5><strong><a href="" style="color:#c3986b;">Table recangule</a></strong></h5>
                <h4 style="color:#c3986b;"><strong>60€</strong></h4>
              </div>
              <!--Card content-->
            </div>
            <!--Card-->

          </div>
          <!--Grid column-->

          <!--Grid column-->
          <div class="col-lg-3 col-md-6 mb-4">

            <!--Card-->
            <div class="card">
              <!--Card image-->
              <div class="view overlay">
                <img src="./img/armoir.png" class="card-img-top" alt="">
                <a><span class="mask rgba-white-slight"></span></a>
              </div>
              <!--Card image-->
              <!--Card content-->
              <div class="card-body text-center">
                <!--Catégorie & Titre-->
                <h5><a href="" style="color:#593718;">Rangement</a></h5>           
                <h5><strong><a href="" style="color:#c3986b;">Armoire</a></strong></h5>             
                <h4 style="color:#c3986b;"><strong>200€</strong></h4>
              </div>
              <!--Card content-->
            </div>
            <!--Card-->

          </div>
          <!--Grid column-->

          <!--Fourth column-->
          <div class="col-lg-3 col-md-6 mb-4">

            <!--Card-->
            <div class="card">
              <!--Card image-->
              <div class="view overlay">
                <img src="./img/chaise.png" class="card-img-top" alt="">
                <a> <span class="mask rgba-white-slight"> </span> </a>
              </div>
              <!--Card image-->
              <!--Card content-->
              <div class="card-body text-center">
                <!--Catégory & Titre-->
                <h5><a href="" style="color:#593718;">Chaises</a></h5>              
                <h5><strong><a href="" style="color:#c3986b;">Chaise de bureau</a></strong></h5>
                <h4 style="color:#c3986b;"><strong>50€</strong></h4>
              </div>
              <!--Card content-->
            </div>
            <!--Card-->
            

          </div>
          <!--Fourth column-->

        </div>
        <!--Grid row-->

      </section>
      <!--Section: Products v.3-->

      <!--Pagination-->
      <nav class="d-flex justify-content-center wow fadeIn">
        <ul class="pagination pg-blue">

          <!--Arrow left-->
          <li class="page-item disabled">
            <a class="page-link" href="#" aria-label="Previous">
              <span aria-hidden="true">&laquo;</span>
              <span class="sr-only">Précédent</span>
            </a>
          </li>

          <li class="page-item active">
            <a class="page-link" href="#" style="background-color: #c3986b;">1
              <span class="sr-only">(current)</span>
            </a>
          </li>
          <li class="page-item">
            <a class="page-link" href="#">2</a>
          </li>
          <li class="page-item">
            <a class="page-link" href="#">3</a>
          </li>
          <li class="page-item">
            <a class="page-link" href="#">4</a>
          </li>
          <li class="page-item">
            <a class="page-link" href="#">5</a>
          </li>

          <li class="page-item">
            <a class="page-link" href="#" aria-label="Next">
              <span aria-hidden="true">&raquo;</span>
              <span class="sr-only">Prochain</span>
            </a>
          </li>
        </ul>
      </nav>
      <!--Pagination-->

    </div>
  </main>
  <!--Main layout-->

  <!--Footer-->
  <footer class="page-footer text-center font-small mt-4 wow fadeIn" style="background-color: #c3986b;">
    <!--Call to action-->
    <div class="pt-4">
      <ul class="navbar-nav mr-auto">
          <li class="nav-item"><a class="nav-link waves-effect" href="#">À propos</a></li>
          <li class="nav-item"><a class="nav-link waves-effect" href="#">Moyens de Paiement</a></li>
          <li class="nav-item"><a class="nav-link waves-effect" href="#">Nous Contacter</a></li>
          <li class="nav-item"><a class="nav-link waves-effect" href="#">Mentions Légales</a></li>
      </ul>
    </div>
    <!--/.Call to action-->
    <hr class="my-4">
    <!-- Social icons -->
    <div class="pb-4">
      <a href="https://www.facebook.com/" target="_blank"><i class="fab fa-facebook-f mr-3"></i></a>
	  <a href="https://twitter.com/" target="_blank"><i class="fab fa-twitter mr-3"></i></a>
      <a href="https://www.youtube.com/" target="_blank"><i class="fab fa-youtube mr-3"></i></a>
      <a href="https://plus.google.com/" target="_blank"><i class="fab fa-google-plus-g mr-3"></i></a>
      <a href="https://dribbble.com/" target="_blank"><i class="fab fa-dribbble mr-3"></i></a>
      <a href="https://pinterest.com/" target="_blank"><i class="fab fa-pinterest mr-3"></i></a>
      <a href="https://gitlab.com/hamza-nejib/parimeub" target="_blank"><i class="fab fa-gitlab mr-3"></i></a>
    </div>
    <!-- Social icons -->
    <!--Copyright-->
    <div class="footer-copyright py-3">&copy; 2024 Copyright:<a href="https://hamza-nejib.github.io/hn/" target="_blank"> Hamza NEJIB </a></div>
    <!--/.Copyright-->
  </footer>
  <!--/.Footer-->

  <!-- SCRIPTS -->
  <script type="text/javascript" src="js/jquery-3.4.1.min.js"></script>
  <script type="text/javascript" src="js/popper.min.js"></script>
  <script type="text/javascript" src="js/bootstrap.min.js"></script>
  <script type="text/javascript" src="js/mdb.min.js"></script>
  <script type="text/javascript">new WOW().init();</script>
</body>
</html>