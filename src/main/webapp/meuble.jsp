<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>    
<!DOCTYPE html>
<html lang="fr">

<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title>PariMeub</title>
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.11.2/css/all.css">
  <link href="css/bootstrap.min.css" rel="stylesheet">
  <link href="css/mdb.min.css" rel="stylesheet">
  <link href="css/style.min.css" rel="stylesheet">
  <link rel="shortcut icon" type="image/x-icon" href="./img/logo.png" />
  <style type="text/css">
    html, body, header, .carousel {height: 60vh;}
	@media (max-width: 740px) {html, body, header, .carousel {height: 100vh;}}
    @media (min-width: 800px) and (max-width: 850px) {html, body, header, .carousel {height: 100vh;}}
  </style>
</head>

<body>
  <!-- Navbar -->
  <nav class="navbar fixed-top navbar-expand-lg navbar-light white scrolling-navbar">
    <div class="container">
      <!-- Brand -->
      <a href="./index.jsp"><img id="logo" src="./img/logo.png"></a>
      <a class="navbar-brand waves-effect" href="./index.jsp"><strong style="color:#593718; font-weight: bold;" >PariMeub</strong></a>
      <!-- Collapse -->
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
        aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <!-- Links -->
      <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <!-- Left -->
        <ul class="navbar-nav mr-auto">
          <li class="nav-item"><a class="nav-link waves-effect" href="./index.jsp">Accueil</a></li>
          <li class="nav-item active"><a class="nav-link waves-effect" href="./meuble.jsp">Meubles<span class="sr-only">(current)</span></a></li>
          <li class="nav-item"><a class="nav-link waves-effect" href="./profil.jsp">Mon Profil</a></li>
          <li class="nav-item"><a class="nav-link waves-effect" href="./contact.jsp">Contact</a></li>
          <li class="nav-item"><a class="nav-link waves-effect" href="./connecter.jsp">Se Connecter</a></li>
        </ul>
        <!-- Right -->
        <ul class="navbar-nav nav-flex-icons">
          <li class="nav-item"><a class="nav-link waves-effect" href="./commande.jsp"><span class="badge brown z-depth-1 mr-1"> 1 </span>
              <i class="fas fa-shopping-cart"></i><span class="clearfix d-none d-sm-inline-block"> Panier </span></a>
          </li>
          <li class="nav-item">
            <a href="https://www.facebook.com/" class="nav-link waves-effect" target="_blank"><i class="fab fa-facebook-f"></i></a>
          </li>
          <li class="nav-item">
            <a href="https://twitter.com" class="nav-link waves-effect" target="_blank"><i class="fab fa-twitter"></i></a>
          </li>
          <li class="nav-item"><a href="https://gitlab.com/hamza-nejib/parimeub" class="nav-link border border-light rounded waves-effect"
              target="_blank">Code<i class="fab fa-gitlab"></i></a>
          </li>
        </ul>
        <% String oklog = (String)session.getAttribute("oklog");
			if(oklog != null){ %>
		<ul class="navbar-nav mr-auto">
		  <li class="nav-item">
		  	<strong class="nav-link waves-effect" style="color:#593718; font-weight: bold;" ><%=oklog%></strong>
		  </li>
		  <li class="nav-item"><a class="nav-link waves-effect" href="./LogoutClient">Se Déconnecter</a></li>
		</ul>
		  <%}%>
      </div>
    </div>
  </nav>
  <!-- Navbar -->

  <!--Main layout-->
  <main class="mt-5 pt-4">
    <div class="container dark-grey-text mt-5">

      <!--Grid row-->
      <div class="row wow fadeIn">

        <!--Grid column-->
        <div class="col-md-6 mb-4">

          <img src="https://mdbootstrap.com/img/Photos/Horizontal/E-commerce/Products/14.jpg" class="img-fluid" alt="">

        </div>
        <!--Grid column-->

        <!--Grid column-->
        <div class="col-md-6 mb-4">

          <!--Content-->
          <div class="p-4">

            <div class="mb-3">
              <a href="">
                <span class="badge purple mr-1">Category 2</span>
              </a>
              <a href="">
                <span class="badge blue mr-1">New</span>
              </a>
              <a href="">
                <span class="badge red mr-1">Bestseller</span>
              </a>
            </div>

            <p class="lead">
              <span class="mr-1">
                <del>$200</del>
              </span>
              <span>$100</span>
            </p>

            <p class="lead font-weight-bold">Description</p>

            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Et dolor suscipit libero eos atque quia ipsa
              sint voluptatibus!
              Beatae sit assumenda asperiores iure at maxime atque repellendus maiores quia sapiente.</p>

            <form class="d-flex justify-content-left">
              <!-- Default input -->
              <input type="number" value="1" aria-label="Search" class="form-control" style="width: 100px">
              <button class="btn btn-primary btn-md my-0 p" type="submit">Add to cart
                <i class="fas fa-shopping-cart ml-1"></i>
              </button>

            </form>

          </div>
          <!--Content-->

        </div>
        <!--Grid column-->

      </div>
      <!--Grid row-->

      <hr>

      <!--Grid row-->
      <div class="row d-flex justify-content-center wow fadeIn">

        <!--Grid column-->
        <div class="col-md-6 text-center">

          <h4 class="my-4 h4">Additional information</h4>

          <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Natus suscipit modi sapiente illo soluta odit
            voluptates,
            quibusdam officia. Neque quibusdam quas a quis porro? Molestias illo neque eum in laborum.</p>

        </div>
        <!--Grid column-->

      </div>
      <!--Grid row-->

      <!--Grid row-->
      <div class="row wow fadeIn">

        <!--Grid column-->
        <div class="col-lg-4 col-md-12 mb-4">

          <img src="https://mdbootstrap.com/img/Photos/Horizontal/E-commerce/Products/11.jpg" class="img-fluid" alt="">

        </div>
        <!--Grid column-->

        <!--Grid column-->
        <div class="col-lg-4 col-md-6 mb-4">

          <img src="https://mdbootstrap.com/img/Photos/Horizontal/E-commerce/Products/12.jpg" class="img-fluid" alt="">

        </div>
        <!--Grid column-->

        <!--Grid column-->
        <div class="col-lg-4 col-md-6 mb-4">

          <img src="https://mdbootstrap.com/img/Photos/Horizontal/E-commerce/Products/13.jpg" class="img-fluid" alt="">

        </div>
        <!--Grid column-->

      </div>
      <!--Grid row-->

    </div>
  </main>
  <!--Main layout-->

  <!--Footer-->
  <footer class="page-footer text-center font-small mt-4 wow fadeIn" style="background-color: #c3986b;">
    <!--Call to action-->
    <div class="pt-4">
      <ul class="navbar-nav mr-auto">
          <li class="nav-item"><a class="nav-link waves-effect" href="#">À propos</a></li>
          <li class="nav-item"><a class="nav-link waves-effect" href="#">Moyens de Paiement</a></li>
          <li class="nav-item"><a class="nav-link waves-effect" href="#">Nous Contacter</a></li>
          <li class="nav-item"><a class="nav-link waves-effect" href="#">Mentions Légales</a></li>
      </ul>
    </div>
    <!--/.Call to action-->
    <hr class="my-4">
    <!-- Social icons -->
    <div class="pb-4">
      <a href="https://www.facebook.com/" target="_blank"><i class="fab fa-facebook-f mr-3"></i></a>
	  <a href="https://twitter.com/" target="_blank"><i class="fab fa-twitter mr-3"></i></a>
      <a href="https://www.youtube.com/" target="_blank"><i class="fab fa-youtube mr-3"></i></a>
      <a href="https://plus.google.com/" target="_blank"><i class="fab fa-google-plus-g mr-3"></i></a>
      <a href="https://dribbble.com/" target="_blank"><i class="fab fa-dribbble mr-3"></i></a>
      <a href="https://pinterest.com/" target="_blank"><i class="fab fa-pinterest mr-3"></i></a>
      <a href="https://gitlab.com/hamza-nejib/parimeub" target="_blank"><i class="fab fa-gitlab mr-3"></i></a>
    </div>
    <!-- Social icons -->
    <!--Copyright-->
    <div class="footer-copyright py-3">&copy; 2024 Copyright:<a href="https://hamza-nejib.github.io/hn/" target="_blank"> Hamza NEJIB </a></div>
    <!--/.Copyright-->
  </footer>
  <!--/.Footer-->

  <!-- SCRIPTS -->
  <script type="text/javascript" src="js/jquery-3.4.1.min.js"></script>
  <script type="text/javascript" src="js/popper.min.js"></script>
  <script type="text/javascript" src="js/bootstrap.min.js"></script>
  <script type="text/javascript" src="js/mdb.min.js"></script>
  <script type="text/javascript">new WOW().init();</script>
</body>
</html>